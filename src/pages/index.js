import * as React from "react"
import { Select } from 'theme-ui'


// styles
const pageStyles = {
  color: "#232129",
  padding: "96px",
  fontFamily: "-apple-system, Roboto, sans-serif, serif",
}

const flex = {
  display:'flex',
  marginBottom: '64px'
}

const filter = {
  marginRight: '12px',
  width: '100%'
}


const headingStyles = {
  marginTop: 0,
  marginBottom: 32,
  maxWidth: 640,
}
const gachiContainer = {
  display: 'flex',
  flexWrap: 'wrap',
  width: '100%',
}
const gachiItem = {
  display: 'flex',
  flexDirection:'column',
  width: '25%',
  alignItems: 'center',
  color: "#232129",
  fontSize: "32px",
  fontWeight: 700,
}

const gachiImage = {
  width: '40%',
  height: 'auto',

}

const headingAccentStyles = {
  color: "#663399",
}

const descriptionStyle = {
  color: "#232129",
  fontSize: "18px",
}


class GachiContainer extends React.Component {

    state = {
      gachies: [
        {
          text: "Test",
          audio: "https://www.gatsbyjs.com/docs/tutorial/",
          description:
              "A great place to get started if you're new to web development. Designed to guide you through setting up your first Gatsby site.",
          image: 'https://s.tcdn.co/0b7/22f/0b722fbb-9ce6-365b-9e01-91161b7e391c/4.png',
          type: 'type1'
        },
        {
          text: "Test",
          audio: "https://www.gatsbyjs.com/docs/tutorial/",
          description:
              "A great place to get started if you're new to web development. Designed to guide you through setting up your first Gatsby site.",
          image: 'https://s.tcdn.co/0b7/22f/0b722fbb-9ce6-365b-9e01-91161b7e391c/4.png',
          type: 'type2'
        },
        {
          text: "Test",
          audio: "https://www.gatsbyjs.com/docs/tutorial/",
          description:
              "A great place to get started if you're new to web development. Designed to guide you through setting up your first Gatsby site.",
          image: 'https://s.tcdn.co/0b7/22f/0b722fbb-9ce6-365b-9e01-91161b7e391c/4.png',
          type: 'type3'
        },
        {
          text: "Test",
          audio: "https://www.gatsbyjs.com/docs/tutorial/",
          description:
              "A great place to get started if you're new to web development. Designed to guide you through setting up your first Gatsby site.",
          image: 'https://s.tcdn.co/0b7/22f/0b722fbb-9ce6-365b-9e01-91161b7e391c/4.png',
          type: 'type4'
        },
        {
          text: "Test",
          audio: "https://www.gatsbyjs.com/docs/tutorial/",
          description:
              "A great place to get started if you're new to web development. Designed to guide you through setting up your first Gatsby site.",
          image: 'https://s.tcdn.co/0b7/22f/0b722fbb-9ce6-365b-9e01-91161b7e391c/4.png',
          type: 'type5'
        },
        {
          text: "Test",
          audio: "https://www.gatsbyjs.com/docs/tutorial/",
          description:
              "A great place to get started if you're new to web development. Designed to guide you through setting up your first Gatsby site.",
          image: 'https://s.tcdn.co/0b7/22f/0b722fbb-9ce6-365b-9e01-91161b7e391c/4.png',
          type: 'type6'
        },
        {
          text: "Test",
          audio: "https://www.gatsbyjs.com/docs/tutorial/",
          description:
              "A great place to get started if you're new to web development. Designed to guide you through setting up your first Gatsby site.",
          image: 'https://s.tcdn.co/0b7/22f/0b722fbb-9ce6-365b-9e01-91161b7e391c/4.png',
          type: 'type7'
        },
      ],
      filtered: [
        {
          text: "Test",
          audio: "https://www.gatsbyjs.com/docs/tutorial/",
          description:
              "A great place to get started if you're new to web development. Designed to guide you through setting up your first Gatsby site.",
          image: 'https://s.tcdn.co/0b7/22f/0b722fbb-9ce6-365b-9e01-91161b7e391c/4.png',
          type: 'type1'
        },
        {
          text: "Test",
          audio: "https://www.gatsbyjs.com/docs/tutorial/",
          description:
              "A great place to get started if you're new to web development. Designed to guide you through setting up your first Gatsby site.",
          image: 'https://s.tcdn.co/0b7/22f/0b722fbb-9ce6-365b-9e01-91161b7e391c/4.png',
          type: 'type2'
        },
        {
          text: "Test",
          audio: "https://www.gatsbyjs.com/docs/tutorial/",
          description:
              "A great place to get started if you're new to web development. Designed to guide you through setting up your first Gatsby site.",
          image: 'https://s.tcdn.co/0b7/22f/0b722fbb-9ce6-365b-9e01-91161b7e391c/4.png',
          type: 'type3'
        },
        {
          text: "Test",
          audio: "https://www.gatsbyjs.com/docs/tutorial/",
          description:
              "A great place to get started if you're new to web development. Designed to guide you through setting up your first Gatsby site.",
          image: 'https://s.tcdn.co/0b7/22f/0b722fbb-9ce6-365b-9e01-91161b7e391c/4.png',
          type: 'type4'
        },
        {
          text: "Test",
          audio: "https://www.gatsbyjs.com/docs/tutorial/",
          description:
              "A great place to get started if you're new to web development. Designed to guide you through setting up your first Gatsby site.",
          image: 'https://s.tcdn.co/0b7/22f/0b722fbb-9ce6-365b-9e01-91161b7e391c/4.png',
          type: 'type5'
        },
        {
          text: "Test",
          audio: "https://www.gatsbyjs.com/docs/tutorial/",
          description:
              "A great place to get started if you're new to web development. Designed to guide you through setting up your first Gatsby site.",
          image: 'https://s.tcdn.co/0b7/22f/0b722fbb-9ce6-365b-9e01-91161b7e391c/4.png',
          type: 'type6'
        },
        {
          text: "Test",
          audio: "https://www.gatsbyjs.com/docs/tutorial/",
          description:
              "A great place to get started if you're new to web development. Designed to guide you through setting up your first Gatsby site.",
          image: 'https://s.tcdn.co/0b7/22f/0b722fbb-9ce6-365b-9e01-91161b7e391c/4.png',
          type: 'type7'
        },
      ]
    };


  filterGachies() {
    console.log('test')
    // filteredGachies = gachies.filter((item) => {
    //   item.type = 'type3'
    // })
    this.setState((prevState) => {
      return {
        filtered: prevState.gachies.filter(item => {
          return item.type == 'type3'
        })
      }
    })
  }
  render() {
    const { filtered } = this.state;
    return <section>
      <div style={{ ...flex }}>
        <div style={filter}>
          <Select>
            <option onClick={ this.filterGachies.bind(this) }
                    >Hello</option>
            <option>Hi</option>
            <option>Beep</option>
            <option>Boop</option>
          </Select>
        </div>
        <div style={filter}>
          <Select
              defaultValue='Hello'>
            <option>Hello</option>
            <option>Hi</option>
            <option>Beep</option>
            <option>Boop</option>
          </Select>
        </div>

      </div>

      <div style={gachiContainer} >



        {filtered.map((item, index) => (
            <GachiListItem item={item} key={index} />
        ))}
      </div>

    </section>


    ;
  }
}


class GachiListItem extends React.Component {


  render() {
    console.log(this.props.item)
    return           <div style={ gachiItem }>
      {/*<a*/}
      {/*  style={linkStyle}*/}
      {/*  href={`${link.url}?utm_source=starter&utm_medium=start-page&utm_campaign=minimal-starter`}*/}
      {/*>*/}
      {/*  {link.text}*/}
      {/*</a>*/}
      <img style={gachiImage}
           alt="G Logo"
           src={this.props.item.image}
      />
      <p>{this.props.item.text}</p>
    </div>
        ;
  }
}
// markup
export default class IndexPage extends React.Component {




  render() {
    return <main style={pageStyles}>
          <title>Home Page</title>

          <h1 style={headingStyles}>
            Gachimuchi
            <br />
            <span style={headingAccentStyles}>— is everything you need! </span>
            <span role="img" aria-label="Party popper emojis">
          🎉🎉🎉
        </span>
          </h1>

          <GachiContainer />

        </main>

  }


}

